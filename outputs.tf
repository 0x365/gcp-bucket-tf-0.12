output "name" {
  value = google_storage_bucket.bucket.name
}

output "link" {
  value = google_storage_bucket.bucket.self_link
}