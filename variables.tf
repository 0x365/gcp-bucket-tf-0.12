variable "environment" {
  description = "In which environment I will create this resource."
}

variable "location" {
  description = "In which region I will create this resource."
}

variable "platform" {
  description = "What component does it belong to?"
}

variable "name" {
  description = "Name for this bucket."
}

variable "storage_class" {
  description = "What kind of storage will I use?"
}

variable "versioning" {
  description = "I wish that my objects have different versions?"
  default     = false
}