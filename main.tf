terraform {
  required_version = ">= 0.12"
}

resource "google_storage_bucket" "bucket" {
  name          = "${var.environment}-${var.platform}-${var.name}"
  location      = var.location
  storage_class = var.storage_class

  versioning {
    enabled = var.versioning
  }
}