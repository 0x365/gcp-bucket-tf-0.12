## Google Storage Bucket Module

Author: Alexander Moreno Delgado
License: MIT License
Summary: This module create a bucket in GCP using terraform 0.12

*Short Name:* bucket

|Variable|Description|
|--|--|
|name|Bucket Name|
|environment|Which environment|
|platform|Software Component|
|storage_class|Options: MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE|
|location|Default: US, Options: US, EU, ASIA (Multi regional) or choose a region|
|versioning|Default: false, Options: true, false|